FROM golang:1.9

RUN apt-get -qy update \
      && apt-get -qy install make gettext rsync \
      && go get -u github.com/golang/dep/cmd/dep

WORKDIR /go/src/gitlab.com/latency.at/latencyBot

COPY Gopkg.* ./
RUN  dep ensure -vendor-only

COPY .  .
RUN cd cmd/bot && CGO_ENABLED=0 go build .

FROM alpine:3.6
RUN  apk add --update ca-certificates && adduser -D user
USER user
COPY --from=0 /go/src/gitlab.com/latency.at/latencyBot/cmd/bot/bot /
COPY fonts /home/user/.fonts
ENTRYPOINT [ "/bot" ]
