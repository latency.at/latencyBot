package latencyBot

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"

	"github.com/prometheus/common/expfmt"
	"github.com/prometheus/common/model"
	"golang.org/x/net/context/ctxhttp"
)

// https://github.com/prometheus/prometheus/blob/master/retrieval/scrape.go
const acceptHeader = `application/vnd.google.protobuf;proto=io.prometheus.client.MetricFamily;encoding=delimited;q=0.7,text/plain;version=0.0.4;q=0.3,*/*;q=0.1`

var userAgentHeader = fmt.Sprintf("LatencyAt Bot/0.0.1")

func Scrape(ctx context.Context, ts time.Time, url string, client *http.Client, latToken string) (model.Samples, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Accept", acceptHeader)
	req.Header.Set("User-Agent", userAgentHeader)
	// req.Header.Set("X-Prometheus-Scrape-Timeout-Seconds", fmt.Sprintf("%f", s.timeout.Seconds()))
	req.Header.Add("Authorization", "Bearer "+latToken)

	resp, err := ctxhttp.Do(ctx, client, req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("server returned HTTP status %s", resp.Status)
	}

	var (
		allSamples = make(model.Samples, 0, 200)
		decSamples = make(model.Vector, 0, 50)
	)
	sdec := expfmt.SampleDecoder{
		Dec: expfmt.NewDecoder(resp.Body, expfmt.ResponseFormat(resp.Header)),
		Opts: &expfmt.DecodeOptions{
			Timestamp: model.TimeFromUnixNano(ts.UnixNano()),
		},
	}

	for {
		if err = sdec.Decode(&decSamples); err != nil {
			break
		}
		allSamples = append(allSamples, decSamples...)
		decSamples = decSamples[:0]
	}

	if err == io.EOF {
		// Set err to nil since it is used in the scrape health recording.
		err = nil
	}
	return allSamples, err
}

func ScrapeAll(client *http.Client, token string, latURL *url.URL, values url.Values, probes []string, timeout time.Duration) (map[string]model.Samples, error) {
	samples := map[string]model.Samples{}

	for _, probe := range probes {
		purl := *latURL
		purl.Host = probe + "." + purl.Host
		purl.RawQuery = values.Encode()

		ctx, cancel := context.WithTimeout(context.Background(), timeout)
		defer cancel()
		ss, err := Scrape(ctx, time.Now(), purl.String(), client, token)
		if err != nil {
			// FIXME: We should probably retry here
			return nil, err
		}
		samples[probe] = ss
	}
	return samples, nil
}
